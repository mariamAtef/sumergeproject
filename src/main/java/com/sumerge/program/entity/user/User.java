package com.sumerge.program.entity.user;

import com.sumerge.program.entity.group.Groupps;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



/** @author Mariam Atef. */

/** User Class Entity. */
@Entity
@NamedQueries({
    @NamedQuery(
      name = "User.findUnsercureData",
      query = "SELECT u.name, u.username, u.role FROM User u where u.isDeleted = false "),
    @NamedQuery(
      name = "User.findALLUnsercureData",
      query = "SELECT u.name, u.username, u.role FROM User u"),
    @NamedQuery(
       name = "User.findByName",
       query = "SELECT  u FROM User u where u.username =:username"),
    @NamedQuery(
       name = "Use.getRole",
       query = "SELECT u.role FROM User u where u.isDeleted = false "),
    @NamedQuery(
      name = "User.getInfo",
      query = "SELECT  u from User u where u.id = :id ")
})
@Table(name = "USERS")
public class User implements Serializable {

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "NAME", length = 40)
  private String name;

  @Column(name = "USERNAME")
  private String username;

  @Column(name = "PASSWORD")
  private String password;

  @Column(name = "ROLE")
  private String role;

  @Column(name = "ISDELETED")
  private boolean isDeleted;

  @ManyToMany
  @JoinTable(
      name = "USERSGROUPS",
      joinColumns = @JoinColumn(name = "USERID"),
      inverseJoinColumns = @JoinColumn(name = "GROUID"))
  private List<Groupps> groups;
  public User() {

  }


  public User(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public String getRole() {
    return role;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(boolean deleted) {
    isDeleted = deleted;
  }

  public List<Groupps> getGroups() {
    return groups;
  }

  public void setGroups(List<Groupps> groups) {
    this.groups = groups;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + ", role='"
        + role
        + '\''
        + ", isDeleted="
        + isDeleted
        + '}';
  }
}
