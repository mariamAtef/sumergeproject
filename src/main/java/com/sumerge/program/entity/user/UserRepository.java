package com.sumerge.program.entity.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sumerge.program.JpaException;
import com.sumerge.program.Objects.UserSecure;
import com.sumerge.program.entity.auditLog.AuditLog;
import com.sumerge.program.entity.group.Groupps;

import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;

import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.sumerge.program.entity.group.Groupps;
import org.apache.log4j.Logger;

/**
 * * @author Mariam Atef.
 * */
@Stateless
public class UserRepository {

  @PersistenceContext private EntityManager em;

  private static final Logger LOGGER = Logger.getLogger(UserRepository.class.getName());

  /**
   * get certain user using his/her id.
   *
   * @param id id of certain user.
   * @return
   */
  public User getCertainUserById(int id) {
    try {
      LOGGER.debug("Entering Get certain user by his Id");
      User user = em.find(User.class, id);
      return user;
    } catch (JpaException ex) {
      LOGGER.debug("Catching Excepting in Getting a certain user by his Id");
      throw new JpaException(ex.getMessage(), ex);
    }
  }

  /**
   * get certain user using his/her id.
   *
   * @param id id of certain user.
   * @return
   */
  @Transactional(rollbackOn = Exception.class)
  public UserSecure getUserById(int id, String usernameLogged) {
    LOGGER.debug("Entering Get certain user by his Id by Query");
    TypedQuery<User> query;
    query = em.createNamedQuery("User.getInfo", User.class).setParameter("id", id);
    User result =  query.getSingleResult();
    System.out.print("user " + result);
    UserSecure userSecure = new UserSecure(result.getName(), result.getUsername(),result.getRole());
    String actionName = "GET a user by Id";
    String actionAuthor = usernameLogged;
    insertinAuditLog(userSecure, actionAuthor, actionName);
    return userSecure;
  }

  /**
   * get certain user using his/her username.
   *
   * @param username username of certain user.
   * @return User
   */
  public User getCertainUserByUsername(String username) {
    try {
      LOGGER.debug("Entering Get certain user by his username");
      TypedQuery<User> query;
      query = em.createNamedQuery("User.findByName", User.class).setParameter("username", username);
      User user =  query.getSingleResult();
      return user;
    } catch (JpaException ex) {
      LOGGER.debug("Catching an exception when Getting certain user by his username");
      throw new JpaException(ex.getMessage(), ex);
    }
  }
  /**
   * get certain user using his/her username.
   *
   * @param username username of certain user.
   * @return UserSecure Object
   */
  @Transactional(rollbackOn = Exception.class)
  public UserSecure getUserByUsername(String username,String usernameLogged) {
    try {
      LOGGER.debug("Entering Get certain user by his username");
      TypedQuery<User> query;
      query = em.createNamedQuery("User.findByName", User.class).setParameter("username", username);
      User user =  query.getSingleResult();
      UserSecure userSecure = new UserSecure(user.getName(), user.getUsername(),user.getRole());
      String actionName = "GET a user by username";
      String actionAuthor = usernameLogged;
      insertinAuditLog(userSecure, actionAuthor, actionName);
      return userSecure;
    } catch (JpaException ex) {
      LOGGER.debug("Catching an exception when Getting certain user by his username");
      throw new JpaException(ex.getMessage(), ex);
    }
  }


  /**
   * Get All unsecure data of the users for normal user.
   * @param usernameLogged username of the user that is logged.
   * @return List of users.
   */
  @Transactional(rollbackOn = Exception.class)
  public List<User> getAllUsersNonSecureDataForNormalUser(String usernameLogged) {
    LOGGER.debug("Entering All unsecure data for normal user");
    TypedQuery<User> query = em.createNamedQuery("User.findUnsercureData", User.class);
    List<User> results = query.getResultList();
    String actionName = "GET ALL Users for Normal user";
    String actionAuthor = usernameLogged;
    insertinAuditLog(results, actionAuthor, actionName);
    return results;
  }

  /**
   * Get All unsecure data of the users for admin user.
   * @param usernameLogged username of the user that is logged.
   * @return List of users.
   */
  @Transactional(rollbackOn = Exception.class)
  public List<User> getAllUsersNonSecureDataForAdmin(String usernameLogged) {
    LOGGER.debug("Entering All unsecure data for Admin user");
    TypedQuery<User> query = em.createNamedQuery("User.findALLUnsercureData", User.class);
    List<User> results = query.getResultList();
    String actionName = "GET ALL Users for Admin user";
    String actionAuthor = usernameLogged;
    insertinAuditLog(results, actionAuthor, actionName);
    return results;
  }

  /**
   * Update username of a certain user.
   *
   * @param user user
   */
  public void updateUsername(User user) {
    try {
      LOGGER.debug("Entering Update username");
      em.getTransaction().begin();
      User wantedUser = getCertainUserById(user.getId());
      wantedUser.setUsername(user.getUsername());
      em.getTransaction().commit();
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * Update the name of certain user using his unique username.
   *
   * @param username unique username of the user.
   * @param newName update name of the user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void updateName(String username, String newName, String usernameLogged) {
    try {
      LOGGER.debug("Entering Update name");
      User wantedUser = getCertainUserByUsername(username);
      String actionName = "Update username ";
      String actionAuthor = usernameLogged;
      wantedUser.setUsername(newName);
      insertinAuditLog(wantedUser, actionAuthor, actionName);
      em.persist(wantedUser);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * Change passoword of certain user.
   *
   * @param newPassword new password.
   * @param username username of the user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void resetPassword(String newPassword, String username) {
    try {
      LOGGER.debug("Entering reset password");
      User wantedUser = getCertainUserByUsername(username);
      String actionName = "Reset Password";
      String actionAuthor = username;
      wantedUser.setPassword(newPassword);
      insertinAuditLog(wantedUser, actionAuthor, actionName);
      em.persist(wantedUser);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * * Hash the pasword of the user before saving in the database.
   *
   * @param input old password
   * @return
   * @throws NoSuchAlgorithmException
   * @throws UnsupportedEncodingException
   * @throws UnsupportedEncodingException
   */
  public static String sha256(String input)
      throws NoSuchAlgorithmException, UnsupportedEncodingException, UnsupportedEncodingException {
    MessageDigest md5 = MessageDigest.getInstance("SHA-256");
    byte[] digest = md5.digest(input.getBytes("UTF-8"));
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < digest.length; ++i) {
      sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1, 3));
    }
    return sb.toString();
  }

  /**
   * Add a new user.
   * @param user User that will be added.
   * @param usernameLogged username of the logged in user.
   */
  @Transactional(rollbackOn = Exception.class)
  /** @param user user that will be added to database. */
  public void addUser(User user, String usernameLogged) {
    try {
      LOGGER.debug("Entering Add new user");
      String actionName = "Add new user";
      String actionAuthor = usernameLogged;
      insertinAuditLog(user, actionAuthor, actionName);
      user.setPassword(sha256(user.getPassword()));
      em.persist(user);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  /**
   * Delete certain user using username.
   * @param username Username of the user that the admin will be delete him.
   * @param usernameLogged Username of the logged user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void deleteUser(String username, String usernameLogged) {
    try {
      LOGGER.debug("Entering Delete user");
      User wantedUser = getCertainUserByUsername(username);
      String actionName = "Delete user";
      String actionAuthor = usernameLogged;
      Boolean isDeleted = true;
      wantedUser.setIsDeleted(isDeleted);
      em.persist(wantedUser);
      insertinAuditLog(wantedUser, actionAuthor, actionName);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * Undo Delete of certain user by his username.
   * @param username Username of the user that the admin will be Undo delete him.
   * @param usernameLogged Username of the logged user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void undoDeleteUser(String username, String usernameLogged) {
    try {
      LOGGER.debug("Entering Undo delete user");
      User wantedUser = getCertainUserByUsername(username);
      String actionName = "UNDO Delete user";
      String actionAuthor = usernameLogged;
      Boolean isDeleted = false;
      wantedUser.setIsDeleted(isDeleted);
      em.persist(wantedUser);
      insertinAuditLog(wantedUser, actionAuthor, actionName);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * Get certai group by Id.
   * @param id Group Id.
   * @param usernameLogged username of logged user.
   * @return
   */
  @Transactional(rollbackOn = Exception.class)
  public Groupps getGroupById(int id, String usernameLogged) {
    LOGGER.debug("Entering Get certain Group by its Id by Query");
    TypedQuery<Groupps> query;
    query = em.createNamedQuery("Groupps.findgroup", Groupps.class).setParameter("id", id);
    Groupps group = (Groupps) query.getSingleResult();
    String actionName = "Get Certain group by ID";
    String actionAuthor = usernameLogged;
    insertinAuditLog(group, actionAuthor, actionName);
    return group;
  }

  /**
   * Add certain user to a certain group.
   * @param username username of the user.
   * @param groupId group Id of the group that the user will be added to.
   * @param usernameLogged Username of the logged user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void addUserToGroup(String username, int groupId, String usernameLogged) {
    LOGGER.debug("Entering Adding new user to a group");
    User wantedUser = getCertainUserByUsername(username);
    Groupps wantedGroup = getGroupById(groupId,usernameLogged);
    System.out.println("GROUPS" + wantedGroup.toString());
    List<Groupps> groups = wantedUser.getGroups();
    String actionName = "Add user in a certain group";
    String actionAuthor = usernameLogged;
    groups.add(wantedGroup);
    em.persist(wantedUser);
    insertinAuditLog(groups, actionAuthor, actionName);
  }

  /**
   * Delete certain user from certain group.
   * @param username username of the user.
   * @param groupId Id of the group that the user will be removed from.
   * @param usernameLogged Username of the logged user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void removeUserFromGroup(String username, int groupId, String usernameLogged) {
    LOGGER.debug("Entering Removing certain user from a group");
    User wantedUser = getCertainUserByUsername(username);
    Groupps wantedGroup = getGroupById(groupId,usernameLogged);
    List<Groupps> groups = wantedUser.getGroups();
    groups.remove(wantedGroup);
    String actionName = "Remove user in a certain group";
    String actionAuthor = usernameLogged;
    em.persist(wantedUser);
    insertinAuditLog(groups, actionAuthor, actionName);
  }

  /**
   * Move certain user from one group to another.
   * @param username username of the user.
   * @param oldGroupId old group id that user will be removed from.
   * @param newGroupId new group id that user will be added to.
   * @param usernameLogged Username of the logged user.
   */
  @Transactional(rollbackOn = Exception.class)
  public void moveMember(String username, int oldGroupId, int newGroupId, String usernameLogged) {
    LOGGER.debug("Entering Move user from one group to another");
    removeUserFromGroup(username, oldGroupId, usernameLogged);
    addUserToGroup(username, newGroupId, usernameLogged);
  }

  /**
   * Insert in the AuditLog.
   * @param o object that will represent the entity (User/Group).
   * @param actionAuthor Author that makes the action.
   * @param actionName Action that the author made.
   */
  public void insertinAuditLog(Object o, String actionAuthor, String actionName) {
    ObjectMapper objectMapper = new ObjectMapper();
    AuditLog auditLog = new AuditLog();
    try {
      auditLog.setEntity(objectMapper.writeValueAsString(o));
      auditLog.setActionName(actionName);
      auditLog.setActionAuthor(actionAuthor);
      Timestamp currentTime = new Timestamp(System.currentTimeMillis());
      auditLog.setActionTime(currentTime);
      em.persist(auditLog);
    } catch (JpaException e) {
      em.getTransaction().rollback();
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }


}
