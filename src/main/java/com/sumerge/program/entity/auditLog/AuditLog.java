package com.sumerge.program.entity.auditLog;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/** @author Mariam Atef. */

/** User Class Entity. */
@Entity
@Table(name = "AUDITLOG")
public class AuditLog implements Serializable {

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "ENTITY")
  private String entity;

  @Column(name = "ACTIONNAME")
  private String actionName;

  @Column(name = "ACTIONTIME")
  private Timestamp actionTime;

  @Column(name = "ACTIONAUTHOR")
  private String actionAuthor;

  /**
   * Constructor for the Auditlog.
   * @param entity User or Group
   * @param actionName Action .
   * @param actionTime Timestamp of the action
   * @param actionAuthor Admin or Normal user who makes the action
   */
  public AuditLog(String entity, String actionName, Timestamp actionTime, String actionAuthor) {
    this.entity = entity;
    this.actionName = actionName;
    this.actionTime = actionTime;
    this.actionAuthor = actionAuthor;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEntity() {
    return entity;
  }

  public void setEntity(String entity) {
    this.entity = entity;
  }

  public String getActionName() {
    return actionName;
  }

  public void setActionName(String actionName) {
    this.actionName = actionName;
  }

  public Timestamp getActionTime() {
    return actionTime;
  }

  public void setActionTime(Timestamp actionTime) {
    this.actionTime = actionTime;
  }

  public String getActionAuthor() {
    return actionAuthor;
  }

  public void setActionAuthor(String actionAuthor) {
    this.actionAuthor = actionAuthor;
  }

  public AuditLog() {

  }

  @Override
  public String toString() {
    return "AuditLog{"
        + "id="
        + id
        + ", entity='"
        + entity
        + '\''
        + ", actionName='"
        + actionName
        + '\''
        + ", actionTime="
        + actionTime
        + ", actionAuthor='"
        + actionAuthor
        + '\''
        + '}';
  }
}
