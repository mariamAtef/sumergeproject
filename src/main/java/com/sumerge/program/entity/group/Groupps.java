package com.sumerge.program.entity.group;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sumerge.program.entity.user.User;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** @author Mariam Atef. */

/** Group Class Entity. */
@Entity
@NamedQueries({
    @NamedQuery(name = "Groupps.findgroup", query = "SELECT g  FROM Groupps g  where g.id = :id "),
    @NamedQuery(name = "Groupps.finGroupByName",
            query = "SELECT g from Groupps g  where g.name = :name "),
    @NamedQuery(name = "Groupps.AllGroups", query = "SELECT g from Groupps g ")
})
@Table(name = "GROUPPS")
public class Groupps implements Serializable {

  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "NAME", length = 40)
  private String name;

  @JsonIgnore
  @ManyToMany
  @JoinTable(
      name = "USERSGROUPS",
      joinColumns = @JoinColumn(name = "GROUID"),
      inverseJoinColumns = @JoinColumn(name = "USERID"))
  private List<User> users;

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Groupps() {

  }

  @Override
  public String toString() {
    return "Group{" + "id=" + id + ", name='" + name + '\'' + '}';
  }
}
