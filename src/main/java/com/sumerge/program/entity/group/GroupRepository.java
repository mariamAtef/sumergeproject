package com.sumerge.program.entity.group;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sumerge.program.JpaException;
import com.sumerge.program.entity.auditLog.AuditLog;
import com.sumerge.program.entity.user.UserRepository;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;

@Stateless
public class GroupRepository {

  @PersistenceContext private EntityManager em;
  UserRepository userRepository;

  private static final Logger LOGGER = Logger.getLogger(GroupRepository.class.getName());

  /**
   * Get All Groups.
   *
   * @param usernameLogged User/Admin logged in.
   * @return List of Groups
   */
  @Transactional(rollbackOn = Exception.class)
  public List<Groupps> getAllGroups(String usernameLogged) {
    LOGGER.debug("Entering Get All groups");
    TypedQuery<Groupps> query;
    query = em.createNamedQuery("Groupps.AllGroups", Groupps.class);
    List<Groupps> groups = query.getResultList();
    String actionName = "Getting all groups";
    String actionAuthor = usernameLogged;
    insertinAuditLog(groups, actionAuthor, actionName);
    return groups;
  }

  /**
   * Get Certain Group by Id.
   *
   * @param id Id of certain group.
   * @param usernameLogged User/Admin logged in.
   * @return A Group.
   */
  @Transactional(rollbackOn = Exception.class)
  public Groupps getGroupById(int id, String usernameLogged) {
    LOGGER.debug("Entering Get certain Group by its Id by Query");
    TypedQuery<Groupps> query;
    query = em.createNamedQuery("Groupps.findgroup", Groupps.class).setParameter("id", id);
    Groupps group = (Groupps) query.getSingleResult();
    String actionName = "Get Certain group by ID";
    String actionAuthor = usernameLogged;
    insertinAuditLog(group, actionAuthor, actionName);
    return group;
  }

  /**
   * Get Certain Group by Name.
   *
   * @param name Name of certain group.
   * @param usernameLogged User/Admin logged in.
   * @return A Group.
   */
  @Transactional(rollbackOn = Exception.class)
  public Groupps getGroupByname(String name, String usernameLogged) {
    LOGGER.debug("Entering Get certain Group by its name by Query");
    TypedQuery<Groupps> query;
    query = em.createNamedQuery("Groupps.finGroupByName", Groupps.class).setParameter("name", name);
    Groupps group = (Groupps) query.getSingleResult();
    String actionName = "Get Certain group by Name";
    String actionAuthor = usernameLogged;
    insertinAuditLog(group, actionAuthor, actionName);
    return group;
  }

  /**
   * Add a group.
   *
   * @param group Group entity.
   * @param usernameLogged Admin logged in.
   */
  @Transactional(rollbackOn = Exception.class)
  public void addGroup(Groupps group, String usernameLogged) {
    try {
      LOGGER.debug("Entering Add new new Group");
      String actionName = "Add new Group";
      String actionAuthor = usernameLogged;
      insertinAuditLog(group, actionAuthor, actionName);
      em.persist(group);
    } catch (JpaException e) {
      em.getTransaction().rollback();
      throw new JpaException(e.getMessage(), e);
    }
  }

  /**
   * Delete certain group.
   *
   * @param groupId Id of certain group.
   * @param usernameLogged Admin logged in.
   */
  @Transactional(rollbackOn = Exception.class)
  public void deleteGroup(int groupId, String usernameLogged) {
    LOGGER.debug("Delete certain group");
    Groupps group = getGroupById(groupId, usernameLogged);
    if (group == null) {
      throw new IllegalArgumentException(
          "Group with ID: " + groupId + "does not exist in the database");
    }
    em.remove(group);
  }

  /**
   * Insert in the AuditLog.
   * @param o object that will represent the entity (User/Group).
   * @param actionAuthor Author that makes the action.
   * @param actionName Action that the author made.
   */
  public void insertinAuditLog(Object o, String actionAuthor, String actionName) {
    ObjectMapper objectMapper = new ObjectMapper();
    AuditLog auditLog = new AuditLog();
    try {
      auditLog.setEntity(objectMapper.writeValueAsString(o));
      auditLog.setActionName(actionName);
      auditLog.setActionAuthor(actionAuthor);
      Timestamp currentTime = new Timestamp(System.currentTimeMillis());
      auditLog.setActionTime(currentTime);
      em.persist(auditLog);
    } catch (JpaException e) {
      em.getTransaction().rollback();
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
