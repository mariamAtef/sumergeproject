package com.sumerge.program;

public class JpaException extends RuntimeException {

  public JpaException(String msg) {
    super(msg);
  }

  public JpaException(Throwable t) {
    super(t);
  }

  public JpaException(String msg, Throwable t) {
    super(msg, t);
  }
}
