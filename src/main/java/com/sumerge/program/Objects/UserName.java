package com.sumerge.program.Objects;

import java.io.Serializable;

public class UserName implements Serializable {
  String oldUsername;
  String newUsername;

  public String getOldUsername() {
    return oldUsername;
  }

  public void setOldUsername(String oldUsername) {
    this.oldUsername = oldUsername;
  }

  public String getNewUsername() {
    return newUsername;
  }

  public void setNewUsername(String newUsername) {
    this.newUsername = newUsername;
  }

  public UserName() {
  }

  @Override
  public String toString() {
    return "UserName{"
        + "oldUsername='"
        + oldUsername
        + '\''
        + ", newUsername='"
        + newUsername
        + '\''
        + '}';
  }
}
