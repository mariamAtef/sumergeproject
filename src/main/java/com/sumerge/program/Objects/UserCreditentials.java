package com.sumerge.program.Objects;

import java.io.Serializable;

public class UserCreditentials implements Serializable {
  private String username;
  private String password;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public UserCreditentials(){

  }

  @Override
  public String toString() {
    return "UserCreditentials{"
        + "username='"
        + username
        + '\''
        + ", password='"
        + password
        + '\''
        + '}';
  }

}
