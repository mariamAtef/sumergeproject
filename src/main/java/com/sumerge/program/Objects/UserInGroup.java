package com.sumerge.program.Objects;

import java.io.Serializable;

public class UserInGroup implements Serializable {
  int groupId;
  String username;

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Override
  public String toString() {
    return "UserInGroup{" + "groupId=" + groupId + ", username='" + username + '\'' + '}';
  }

  public UserInGroup() {
  }
}
