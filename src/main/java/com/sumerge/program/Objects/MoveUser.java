package com.sumerge.program.Objects;

import java.io.Serializable;

public class MoveUser implements Serializable {
  int oldGroupId;
  int newGroupId;
  String username;

  public int getOldGroupId() {
    return oldGroupId;
  }

  public void setOldGroupId(int oldGroupId) {
    this.oldGroupId = oldGroupId;
  }

  public int getNewGroupId() {
    return newGroupId;
  }

  public void setNewGroupId(int newGroupId) {
    this.newGroupId = newGroupId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public MoveUser() {
  }

  @Override
  public String toString() {
    return "MoveUser{"
        + "oldGroupId="
        + oldGroupId
        + ", newGroupId="
        + newGroupId
        + ", username='"
        + username
        + '\''
        + '}';
  }
}
