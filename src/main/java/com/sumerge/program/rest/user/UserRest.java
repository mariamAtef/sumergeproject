package com.sumerge.program.rest.user;

import com.sumerge.program.Objects.MoveUser;
import com.sumerge.program.Objects.UserCreditentials;
import com.sumerge.program.Objects.UserInGroup;
import com.sumerge.program.Objects.UserName;
import com.sumerge.program.entity.user.User;
import com.sumerge.program.entity.user.UserRepository;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("users")
public class UserRest {
  private static final Logger LOGGER = Logger.getLogger(UserRest.class.getName());
  @EJB private UserRepository userRepository;

  @Context HttpServletRequest request;

  @Context SecurityContext securityContext;

  @GET
  public Response getAllUsers() {
    try {
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin) {
        LOGGER.debug("Authorized Entering Get All users");
        return Response.ok()
            .entity(userRepository.getAllUsersNonSecureDataForAdmin(usernameLogged))
            .build();
      } else {
        LOGGER.debug("UNAuthorized Entering Get All users");
        return Response.ok()
            .entity(userRepository.getAllUsersNonSecureDataForNormalUser(usernameLogged))
            .build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @GET
  @Path("{id}")
  public Response getCertainUserById(@PathParam("id") int id) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      return Response.ok().entity(userRepository.getUserById(id, usernameLogged)).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @GET
  @Path("/name/{username}")
  public Response getCertainUserByUsername(@PathParam("username") String username) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      return Response.ok().entity(userRepository.getUserByUsername(username, usernameLogged)).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @POST
  @Path("add")
  public Response addUser(User user) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      boolean isAdmin = securityContext.isUserInRole("admin");
      if (isAdmin) {
        LOGGER.debug("Authorized Adding new User");
        userRepository.addUser(user, usernameLogged);
      } else {
        LOGGER.debug("UNAuthorized Adding new User");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
      return Response.ok().build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @PUT
  @Path("reset")
  public Response resetPassword(UserCreditentials userCreditentials) {
    try {
      String username = userCreditentials.getUsername();
      String password = userCreditentials.getPassword();
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (usernameLogged.equals(username)|| isAdmin ) {
        LOGGER.debug("Authorized reseting password");
        userRepository.resetPassword(password, username);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized reseting password");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }

    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @PUT
  @Path("updateusername")
  public Response updateUsername(UserName usernames) {
    try {
      String oldUsername = usernames.getOldUsername();
      String newName = usernames.getNewUsername();
      String usernameLogged = securityContext.getUserPrincipal().toString();
      boolean isAdmin = securityContext.isUserInRole("admin");
      if (isAdmin &&  (!usernames.equals("admin"))) {
        LOGGER.debug("Authorized update name");
        userRepository.updateName(oldUsername, newName, usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized update name");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }

    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @DELETE
  @Path("{username}")
  public Response deleteUser(@PathParam("username") String username) {
    try {
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin &&  (! username.equals("admin"))) {
        LOGGER.debug("Authorized Delete user");
        userRepository.deleteUser(username,usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized Delete user");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @PUT
  @Path("{username}")
  public Response undoDeleteUser(@PathParam("username") String username) {
    try {
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin) {
        LOGGER.debug("Authorized UNDO Delete user");
        userRepository.undoDeleteUser(username,usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized UNDO Delete user");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @POST
  @Path("addToGroup")
  public Response addToGroup(UserInGroup userInGroup) {
    try {
      String username = userInGroup.getUsername();
      int groupId = userInGroup.getGroupId();
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin) {
        LOGGER.debug("Authorized Add user to a group");
        userRepository.addUserToGroup(username, groupId, usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized Add user to a group");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @DELETE
  @Path("removeFromGroup")
  public Response removeFromGroup(UserInGroup userInGroup) {
    try {
      String username = userInGroup.getUsername();
      int groupId = userInGroup.getGroupId();
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin) {
        LOGGER.debug("Authorized Remove user to a group");
        userRepository.removeUserFromGroup(username, groupId, usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized Remove user to a group");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  @PUT
  @Path("moveFromToGroup")
  public Response MoveFromToNewGroup(MoveUser moveUser) {
    try {
      String username = moveUser.getUsername();
      int oldGroupId = moveUser.getOldGroupId();
      int newGroupId = moveUser.getNewGroupId();
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin) {
        LOGGER.debug("Authorized Move user from group to group");
        userRepository.moveMember(username, oldGroupId, newGroupId, usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized Move user from group to group");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }
}
