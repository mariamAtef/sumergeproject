package com.sumerge.program.rest.group;

import com.sumerge.program.entity.group.Groupps;
import com.sumerge.program.entity.group.GroupRepository;
import com.sumerge.program.rest.user.UserRest;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.Logger;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("groups")
public class GroupRest {
  private static final Logger LOGGER = Logger.getLogger(UserRest.class.getName());
  @EJB private GroupRepository groupRepository;

  @Context HttpServletRequest request;

  @Context SecurityContext securityContext;

  /**
   * Get ALL Groups.
   * @return response.
   */
  @GET
  public Response getAllGroups() {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      LOGGER.debug("Authorized Entering Get All groups");
      return Response.ok().entity(groupRepository.getAllGroups(usernameLogged)).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Get certain Group by Id.
   * @param id Id of the wanted group.
   * @return response.
   */
  @GET
  @Path("{id}")
  public Response getCertainUserById(@PathParam("id") int id) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      return Response.ok().entity(groupRepository.getGroupById(id, usernameLogged)).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Get certain Group by name.
   * @param name Name of the wanted group.
   * @return response.
   */
  @GET
  @Path("/name/{name}")
  public Response getCertainUserByUsername(@PathParam("name") String name) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      return Response.ok().entity(groupRepository.getGroupByname(name, usernameLogged)).build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Add a new Group.
   * @param group Group Entity.
   * @return response.
   */
  @POST
  public Response addGroup(Groupps group) {
    try {
      String usernameLogged = securityContext.getUserPrincipal().toString();
      boolean isAdmin = securityContext.isUserInRole("admin");
      if (isAdmin) {
        LOGGER.debug("Authorized Adding new Group");
        groupRepository.addGroup(group, usernameLogged);
      } else {
        LOGGER.debug("UNAuthorized Adding new Group");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
      return Response.ok().build();
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Delete certain group by it's Id.
   * @param id Id of the wanted group.
   * @return reponsee.
   */
  @DELETE
  @Path("{id}")
  public Response deleteGroup(@PathParam("id") int id) {
    try {
      boolean isAdmin = securityContext.isUserInRole("admin");
      String usernameLogged = securityContext.getUserPrincipal().toString();
      if (isAdmin && (id != 1)) {
        LOGGER.debug("Authorized Delete Group");
        groupRepository.deleteGroup(id, usernameLogged);
        return Response.ok().build();
      } else {
        LOGGER.debug("UNAuthorized Delete Group");
        return Response.status(Response.Status.UNAUTHORIZED).build();
      }
    } catch (Exception e) {
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }
}
