package com.sumerge.program.rest.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Mariam Atef.
 **/

/**
 * Main Application Path.
 */
@ApplicationPath("/userManagmentSystem")
public class MyApplication extends Application {
}
