import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.sumerge.program.entity.user.User;
import com.sumerge.program.entity.user.UserRepository;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RunWith(JUnit4.class)
public class UserRestIT {
  UserRepository userRepository;

  @Before
  public void init() {
    User userTest = new User();
    userTest.setName("TestAdmin");
    userTest.setIsDeleted(false);
    userTest.setName("TestAdmin");
    userTest.setPassword("user");
    userTest.setRole("admin");

    userRepository.addUser(userTest, "admin");
  }

  @Test
  public void GetAllUsers() {

    Client client = ClientBuilder.newClient();
    HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "admin");
    List<User> result =
        client
            .target("http://localhost:8880/userManagmentSystem/users")
            .register(feature)
            .register(JacksonJsonProvider.class)
            .request(MediaType.APPLICATION_JSON)
            .get(List.class);
    Assert.assertNotNull(result);
    Assert.assertTrue("Test fails", result.size() > 0);
  }

  @Test
  public void getCertaiUser() {
    Client client = ClientBuilder.newClient();
    HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "admin");
    User result =
        client
            .target("http://localhost:8880/userManagmentSystem/users/TestAdmin")
            .register(feature)
            .register(JacksonJsonProvider.class)
            .request(MediaType.APPLICATION_JSON)
            .get(User.class);
    Assert.assertNotNull(result);
    Assert.assertTrue("Test fails", result.getUsername().equals("TestAdmin"));
  }
}
